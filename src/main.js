const electron = require('electron');

const path = require('path');

const { app, BrowserWindow, Tray, Menu, ipcMain } = electron;

let mainWindow;

app.on('ready', _ => {

  console.log('Ready!');

  // Main app window
  mainWindow = new BrowserWindow({
    height: 200,
    width: 500,
    resizable: false
  });

  // Start electron with Chrome dev tools
  // mainWindow.openDevTools();

  const name = electron.app.getName();

  // App menu
  const template = [{
    label: name,
    submenu: [{
      label: `About ${name}`,
      click: _ => {
        console.log('about clicked!')
      }
    }, {
      type: 'separator'
    }, {
      label: 'Close',
      click: _ => { app.quit() },
      accelerator: 'CommandOrControl+q'
    }, {
      label: 'Minimize',
      role: 'minimize'
    }]
  }, {
    label: 'Learn More',
    click () { require('electron').shell.openExternal('https://electron.atom.io') }
  }];
  const menu = Menu.buildFromTemplate(template);
  mainWindow.setMenu(menu);
  //mainWindow.setMenu(null);

  // Tray menu
  const tray = new Tray(path.join(__dirname, 'img', 'app_tray.png'));
  const tray_template = [{
    label: 'Tacos dorados de poio',
    click: _ => console.log('tacos dorados de poio')
  }, {
    label: 'Show app',
    click: _ => { mainWindow.show() }
  }, {
    label: 'Close',
    click: _ => { app.quit() },
    accelerator: 'CommandOrControl+q'
  }];
  const context = Menu.buildFromTemplate(tray_template);
  tray.setToolTip(name);
  tray.setContextMenu(context);

  // Load main HTML
  mainWindow.loadURL(`file://${__dirname}/index.html`);

  // Hide window on minimize
  mainWindow.on('minimize',function(event){
    event.preventDefault()
    mainWindow.hide();
  });

  // Delete instance to avoid garbage
  mainWindow.on('closed', _ => {
    console.log('Closed!')
    mainWindow = null
  });
});

ipcMain.on('clean-furby', _ => {
  console.log('Got clean furby');
})
