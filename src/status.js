const fs = require('fs');
const path = require('path');
const exec = require('child_process').exec;
const os = require('os');


const electron = require('electron');
const ipc = electron.ipcRenderer;

function isDir(dir) {
  try {
    return fs.lstatSync(dir).isDirectory();
  } catch (e) {
    return false;
  }
}

function checkGitStatus(dir) {
  exec('git status', {
    cwd: dir
  }, (err, stdout, stderr) => {
    console.log('err', err);
    console.log('stdout', stdout);
    console.log('stderr', stderr);

    if (err) {
      return setStatus('Unknown');
    }

    if (/nothing to commit/.test(stdout)) {
      ipc.send('clean-furby');
      return setStatus('Clean');
    }

    return setStatus('Dirty');
  })
}

function formatDir(dir) {
  return /^~/.test(dir) ?
    os.homedir() + dir.substr(1).trim() :
    dir.trim();
}

function removeStatus() {
  document.getElementById('image-status').src = path.join(__dirname, 'img', `status-Unknown.svg`);
  const el = document.getElementById('status-text');
  el.innerHTML = '';
  el.classList.remove('Unknown', 'Clean', 'Dirty');
  return el;
}

function setStatus(status) {
  const el = removeStatus();
  document.getElementById('image-status').src = path.join(__dirname, 'img', `status-${status}.svg`);
  el.innerHTML = status;
  el.classList.add(status);
}

let timer;
document.getElementById('input').addEventListener('keyup', evt => {
  removeStatus();
  clearTimeout(timer);
  timer = setTimeout( _ => {
    const dir = formatDir(evt.target.value);
    if (isDir(dir)) {
      checkGitStatus(dir);
    }
  }, 500)
})
checkGitStatus(document.getElementById('input').value);
